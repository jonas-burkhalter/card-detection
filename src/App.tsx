import {
  IonApp,
  IonCol,
  IonContent,
  IonPage,
  IonRouterOutlet,
  IonRow,
  IonSplitPane,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Redirect, Route } from "react-router-dom";

import Header from "./components/common/Header";
import Navigation from "./components/common/Navigation";

import UnoImage from "./components/uno/views/Image";
import UnoPhoto from "./components/uno/views/Photo";
import UnoVideo from "./components/uno/views/Video";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";

const App: React.FC = () => {

  return (
    <IonApp id="app">
      <IonReactRouter>
        <IonSplitPane contentId="main" when="null">
        <Navigation />

          <IonRouterOutlet id="main">
            <IonPage>
              <Header />

              <IonContent fullscreen>
                <IonRow id="background-content">
                  <IonCol offsetMd="2" sizeMd="8">
                    <Route path="/" exact={true}>
                      <Redirect to="/uno/image" />
                    </Route>

                    <Route path="/uno/image" exact={true}>
                      <UnoImage />
                    </Route>

                    <Route path="/uno/photo" exact={true}>
                      <UnoPhoto />
                    </Route>

                    <Route path="/uno/video" exact={true}>
                      <UnoVideo />
                    </Route>

                  </IonCol>
                </IonRow>
              </IonContent>
            </IonPage>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
