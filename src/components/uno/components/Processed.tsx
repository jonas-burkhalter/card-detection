import { IonButton, IonCardContent, IonCol, IonRow } from "@ionic/react";
import { useEffect } from "react";
import invert from 'invert-color';
import convertCssColorNameToHex from 'convert-css-color-name-to-hex';

import UnoPrediction from "../models/UnoPrediction";


interface IProcessed {
  clear: () => void,
  image: string,
  processed: any
}

const Processed = ({ clear, image, processed }: IProcessed) => {
  useEffect(() => {
    if (image && processed) {
      draw();
    }
  }, [image, processed])

  function addTextToCanvas(context: any, unoPrediction: UnoPrediction, prediction: any) {
    context.beginPath();

    context.font = "30px Arial";
    context.textAlign = "center";
    context.fillStyle = invert(convertCssColorNameToHex(unoPrediction.getColor()));
    context.globalAlpha = 1;

    context.fillText(unoPrediction.getValue(), prediction.x, prediction.y - 15);
    context.fillText(unoPrediction.getPercentage() + "%", prediction.x, prediction.y + 15);

    context.closePath();
  }

  function addRectangle(context: any, unoPrediction: UnoPrediction, prediction: any) {
    context.beginPath();

    console.debug("draw", prediction.x, prediction.y, prediction.width, prediction.height, prediction.class);

    context.globalAlpha = 0.5;
    context.fillStyle = unoPrediction.getColor();

    context.fillRect(
      prediction.x - prediction.width / 2,
      prediction.y - prediction.height / 2,
      prediction.width,
      prediction.height
    );

    context.stroke();

    context.closePath();
  }

  function draw() {
    const canvas: any = document.getElementById('uno-image-canvas');
    const context = canvas.getContext('2d');

    const img = new Image;
    img.src = image;
    img.onload = () => {
      context.drawImage(img, 0, 0, processed?.image?.width, processed?.image?.height);


      processed.predictions.forEach(prediction => {
        const unoPrediction = new UnoPrediction(prediction.confidence, prediction.class);

        addRectangle(context, unoPrediction, prediction);

        addTextToCanvas(context, unoPrediction, prediction);
      });
    }
  }

  return (
    <>
      {processed && (
        <IonCardContent>
          <IonRow>
            <IonCol>
              <canvas id="uno-image-canvas" height={processed?.image?.height} width={processed?.image?.width} />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonButton onClick={clear}>clear</IonButton>
          </IonRow>
        </IonCardContent>
      )}
    </>
  );
};

export default Processed;
