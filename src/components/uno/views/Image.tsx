import { IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonLoading, IonRow } from "@ionic/react";
import { unoService } from "../services/UnoService";
import { base64FromFile } from '../hooks/PhotoGallery';
import { useState } from "react";
import Processed from "../components/Processed";

const UnoImageView: React.FC = () => {
  const [image, setImage] = useState<File>(null);
  const [loading, setLoading] = useState(false);
  const [processed, setProcessed] = useState(null);

  function change(fileChangeEvent: any) {
    console.debug(fileChangeEvent.target.files[0])
    setImage(fileChangeEvent.target.files[0])
  }

  function clear(): void {
    setImage(null);
    setProcessed(null);
  }

  async function detection() {
    setLoading(true);

    const base64Data = await base64FromFile(image);

    unoService.post(base64Data).then((response) => {
      console.info(response);
      setProcessed(response);
    }).finally(()=>{
      setLoading(false)
    });
  }

  return (
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>image</IonCardTitle>
      </IonCardHeader>

      {!processed && (
        <IonCardContent>

          <IonRow>
            <input type="file" onChange={change} accept="image/*" />
          </IonRow>

          <IonLoading isOpen={loading} />

          <IonRow>
            <IonButton disabled={image === null || loading} onClick={detection}>process</IonButton>
          </IonRow>
        </IonCardContent>
      )}

      <Processed clear={clear} image={image ? URL.createObjectURL(image) : null} processed={processed} />
    </IonCard>
  );
};

export default UnoImageView;
