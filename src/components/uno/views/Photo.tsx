import { IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonLoading, IonRow } from "@ionic/react";
import { unoService } from "../services/UnoService";
import { base64FromPath, usePhotoGallery } from '../hooks/PhotoGallery';
import { useEffect, useState } from "react";
import Processed from "../components/Processed";

const UnoImageView: React.FC = () => {
  const { photo, takePhoto } = usePhotoGallery();
  const [loading, setLoading] = useState(false);
  const [processed, setProcessed] = useState(null);

  useEffect(() => {
    if (photo) {
      detection()
    }
  }, [photo])

  function clear(): void {
    setProcessed(null);
  }

  async function detection() {
    setLoading(true);

    const base64Data = await base64FromPath(photo.webviewPath!);

    unoService.post(base64Data).then((response) => {
      console.debug(response);
      setProcessed(response);
    }).finally(()=>{
      setLoading(false)
    });
  }

  async function take() {
    await takePhoto();
  }

  return (
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>photo</IonCardTitle>
      </IonCardHeader>
      
      <IonLoading isOpen={loading} />

      {!processed && !loading && (
        <IonCardContent>
          <IonRow>
            <IonButton onClick={take}>take image</IonButton>
          </IonRow>
        </IonCardContent>
      )}

      <Processed clear={clear} image={photo ? photo.webviewPath : null} processed={processed} />
    </IonCard>
  );
};

export default UnoImageView;
