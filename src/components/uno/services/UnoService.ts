import { AxiosRequestConfig } from "axios";
import BaseServie from "../../../services/BaseService";

export default class UnoService extends BaseServie {
  private API_KEY: string = "twAd4Wqzp9zwpscja84N";
  private RESSOURCE_URL: string = "https://detect.roboflow.com/cartes-annotees/4";
  private config: AxiosRequestConfig = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    method: "POST",
    params: {
      api_key: this.API_KEY
    },
  }

  constructor() {
    super();
    console.debug("UnoService::constructor", this.RESSOURCE_URL);
  }

  public async post(image: any): Promise<any> {
    return super.POST(this.RESSOURCE_URL, image, this.config);
  }
}

export const unoService = new UnoService();
