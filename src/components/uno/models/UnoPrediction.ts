export default class UnoPrediction {
  private color: string;
  private percentage: number;
  private value: string;

  constructor(percentage: number, value: string) {
    console.debug("UnoPrediction::constructor", value);
    this.color = this.convertClassToColor(value);
    this.percentage = percentage;
    this.value = this.convertClassToValue(value);
  }

  public getColor() {
    return this.color;
  }
  
  public getPercentage() {
    return Math.round(this.percentage * 100);
  }
  
  public getValue() {
    return this.value;
  }

  private convertClassToColor(predictionClass: string): string {
    switch (predictionClass.charAt(0)) {
      case '0':
        return 'red'
      case '1':
        return 'yellow'
      case '2':
        return 'green'
      case '3':
        return 'blue'
      case '4':
        return 'black'
      default:
        break;
    }
  }

  private convertClassToValue(predictionClass: string): string {
    if (predictionClass.charAt(0) !== '4') {
      switch (predictionClass.charAt(1)) {
        case 'A':
          return 'reverse';
        case 'B':
          return 'skip';
        case 'C':
          return 'draw two';
        default:
          return predictionClass.charAt(1);
      }
    } else {
      switch (predictionClass.charAt(1)) {
        case '0':
          return 'wild draw four';
        case '1':
          return 'wild';
      }
    }

  }
}