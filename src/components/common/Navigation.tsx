import {
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle,
} from "@ionic/react";

import { camera, image } from 'ionicons/icons';

import { useLocation } from "react-router-dom";

const NavigationView: React.FC = () => {
    const location = useLocation();

    const navigations = [
        {
            url: "/uno/image",
            icon: image,
            label: "image",
        },
        {
            url: "/uno/photo",
            icon: camera,
            label: "photo",
        }
    ]

    return (
        <IonMenu contentId="main" type="overlay">
            <IonContent>
                <IonList id="navigation">
                    {navigations?.map((item, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem
                                    color={location.pathname === item.url ? "primary" : ""}
                                    routerLink={item.url}
                                    routerDirection="none"
                                    lines="none"
                                    detail={false}
                                >
                                    <IonIcon icon={item.icon} slot="start"/>
                                    <IonLabel>{item.label}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        );
                    })}
                </IonList>
            </IonContent>
        </IonMenu>
    );
};

export default NavigationView;
