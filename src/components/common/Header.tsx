import {
    IonHeader,
    IonToolbar,
    IonButtons,
    IonMenuButton,
    IonTitle,
} from "@ionic/react";

import React from "react";

const HeaderView: React.FC = () => {
    return (
        <IonHeader>
            <IonToolbar color="secondary">
                <IonButtons slot="start">
                    <IonMenuButton />
                </IonButtons>
                <IonTitle>Uno Card detection system with Image processing</IonTitle>
            </IonToolbar>
        </IonHeader>
    );
};

export default HeaderView;
