# card detection

## task
Detect UNO cards lying on the table and classify their value. This project is done with machine learning.

## approach
As a none machine learning expert, I search for an easy to use detection method and found the following.

### roboflow
Roboflow empowers developers to build their own computer vision applications, no matter their skillset or experience. We provide all of the tools needed to convert raw images into a custom trained computer vision model and deploy it for use in applications. Today, Roboflow supports object detection and classification models. 
([roboflow documentation](https://docs.roboflow.com/))

#### pre-trained model
For my project I found this [cartes annotées]( https://universe.roboflow.com/unodetection/cartes-annotees) model for uno card detection. 
With a training set of 602 images and around 200 images to validate and test the model. 

## implementation
Then I create a small Javascript application and used the [Hosted API]( https://universe.roboflow.com/unodetection/cartes-annotees/model/4) of roboflow for the detection.

### image options
First I started with a fix image to process, then I made it uploadable and lastly I added the option to take a photo to process.

### response
The response from roboflow is easy to understand. It contains the time it took to process and some image meta data. For the prediction it returns an array of centered rectangles, the probability and a class.
The class has two characters, the first defines the color of the card and the second the value.
#### colors
-	0 -> red
-	1 -> yellow
-	2 -> green
-	3 -> blue
-	4 -> black

#### values
For the normal card the values mean
- 0-9 (number card)
    - <img src="readme/one.jpg" title="number card" height="200px" width="130px">

- A (reverse card)
    - <img src="readme/reverse.png" title="reverse card" height="200px" width="130px">

- B (skip card)
    - <img src="readme/skip.png" title="skip card" height="200px" width="130px">

- C (draw two card)
     - <img src="readme/draw_two.jpeg" title="draw two card" height="200px" width="130px">

For the special cards the values mean
- 0 (wild draw four card)
    - <img src="readme/wild_draw_four.jpeg" title="wild draw four card" height="200px" width="130px">

- 1 (wild card)
    - <img src="readme/wild.png" title="wild card" height="200px" width="130px">

#### exampe
```json
{
    "time": 3.1264248229999794,
    "image": {
        "width": 1966,
        "height": 1753
    },
    "predictions": [
        {
            "x": 1513.5,
            "y": 380,
            "width": 89,
            "height": 96,
            "confidence": 0.8081408739089966,
            "class": "04"
        },
        ...
    ]
}
```

## application
The information form the response is processed and drawn on to the image. The rectangles get the color from the predicted class and contain the value and the probability.

![application](readme/application.png "application")