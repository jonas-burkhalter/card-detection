import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'card-detection',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
